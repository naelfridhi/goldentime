import { WMWebAppPage } from './app.po';

describe('wm-web-app App', () => {
  let page: WMWebAppPage;

  beforeEach(() => {
    page = new WMWebAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
