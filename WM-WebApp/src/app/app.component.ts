import { Component } from '@angular/core';
import { SharedService } from './providers/shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	public layout:boolean = true;
	
	public user:any;
	constructor(sharedService:SharedService) {
		this.user = sharedService.getUser();
	}
}