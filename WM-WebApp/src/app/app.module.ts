import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { FormsModule }          from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent }          from './app.component';
import { HomeComponent }   from './pages/home/home.component';
import { LoginComponent }   from './pages/login/login.component';
import { LogoutComponent }   from './pages/logout/logout.component';
import { ClientComponent }   from './pages/client/client.component';
import { PageNotFoundComponent } from './pages/not-found/not-found.component';
import { SharedService } 		from './providers/shared';
const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'client', component: ClientComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
]; 
@NgModule({
  imports: [
	BrowserModule,
	FormsModule,
	HttpModule,
	RouterModule.forRoot(appRoutes)
  ],
  declarations: [
	AppComponent,
	HomeComponent,
	LoginComponent,
	LogoutComponent,
	ClientComponent,
	PageNotFoundComponent
  ],
   providers: [
	SharedService,
	],
  bootstrap: [ AppComponent ]
})
export class AppModule { }