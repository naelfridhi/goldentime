import { Component, Host, Input } from '@angular/core';
import { SharedService } from '../../providers/shared';
import { AppComponent } from '../../app.component'
import { Router } from '@angular/router'
@Component({
  templateUrl: './login.component.html',
})
export class LoginComponent {
  
	public submitted:boolean;
	public user = {username:'',	password:'',	type:null};
	public incorrect = false;
  constructor(@Host() parent: AppComponent,public sharedService:SharedService,private router: Router) {
    parent.layout = false;
	if(Object.keys(this.sharedService.getUser()).length != 0) {
		this.router.navigateByUrl(this.sharedService.HomePage);
		this.sharedService.redirect(this.sharedService.HomePage);
	}
  }

  public onSubmit(values:Object,isValid: boolean):void {
    this.submitted = true;
	this.sharedService.getUserFromAPI(this.user.username,this.user.password).subscribe(
	res => {
		let data = res.json();
		console.log(data);
		if(Object.keys(data).length == 0) {
			this.submitted = false;
			this.incorrect = true;
		}else {
			this.sharedService.setUser(data);
			this.sharedService.setUserType(this.user.type);
			this.sharedService.redirect(this.sharedService.HomePage);
		}
	},
	err => { 
		this.submitted = false;
		this.incorrect = true;
	}
	);
  } 
}