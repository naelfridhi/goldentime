    import { Component, Host } from '@angular/core';
	import { AppComponent } from '../../app.component'
    @Component({
      template: '<h2>Page not found</h2>'
    })
    export class PageNotFoundComponent {
		constructor(@Host() parent: AppComponent) {
			parent.layout = false;
		}
	}