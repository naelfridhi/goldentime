import { Component } from '@angular/core';
import { SharedService } from '../../providers/shared';
import { Router } from '@angular/router'
@Component({
  templateUrl: './client.component.html',
})
export class ClientComponent {
	private reservation:any = {};
	private clients:Array<Object> = [];
	constructor(private sharedService:SharedService,private router: Router) {
		
				if(Object.keys(this.sharedService.getUser()).length == 0) this.router.navigateByUrl(this.sharedService.LoginPage);
		
		
		
		
		let user = this.sharedService.getUser();
		this.sharedService.getReservationsByGuichetier(user.id).subscribe(
		res => {
			this.reservation = res.json();
			for (var key in this.reservation ) {
				if (key == "client")
					this.clients.push(this.reservation[key]);
			}
		},
		err => {
			
		});
		
		
	}

}