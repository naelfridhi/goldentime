import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../providers/shared';
import { Router } from '@angular/router'
@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
	private user:any = {};
	public active:boolean;
	public submited:boolean = false;
	public error:boolean = false;
	public done:boolean = false;
	constructor(private sharedService:SharedService,private router: Router) {
		this.user = sharedService.getUser(); 
		this.active = this.user.active;
	}
  public onSubmit(values:Object,isValid: boolean):void {
	  this.submited = true;
	  console.log(values);
		this.user['active'] = values['state'];
		this.user['counterNumber'] = values['counter'];
		this.sharedService.UpdateGuichier(this.user).subscribe(
	res => {
		let data = res.json();
		if(Object.keys(data).length == 0) {
			this.submited = false;
			this.error = true;
		}else {
			this.done = true;
		}
	},
	err => { 
		this.submited = false;
		this.error = true;
	}
	);
		//active = (f.value.active == 'true')?true:false;
	}
	ngOnInit() {
		if(Object.keys(this.user).length == 0) this.router.navigateByUrl(this.sharedService.LoginPage);
	}
}