import { Component, Host, Input } from '@angular/core';
import { SharedService } from '../../providers/shared';
import { AppComponent } from '../../app.component'
import { Router } from '@angular/router'
@Component({
  template: 'Déconnexion...',
})
export class LogoutComponent {

  constructor(@Host() parent: AppComponent,public sharedService:SharedService) {
    parent.layout = false;
	this.sharedService.setUser({});
	this.sharedService.redirect(this.sharedService.LoginPage);
  }

}