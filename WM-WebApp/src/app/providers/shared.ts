import {Injectable} from '@angular/core';  
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';


@Injectable()
export class SharedService {
	url:string;
	public HomePage = "/home";
	public LoginPage = "/login";

	user:any = {};
	constructor(public http:Http) {
		this.url = "http://127.0.0.1:4200/wait-management";
	}
	
	getUserFromAPI(username:string,password:string) {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		return this.http.post(this.url+`/guichier/login`,{"username": username, "password": password},options);
    }
	
	UpdateGuichier(guichier) { 
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		let ops = {}
		for(var key in guichier) {
			if ( key != "type" && key != "service" && key != "reservations" ) {
					ops[key] = guichier[key];
			}
		}
		console.log(ops);
		return this.http.put(this.url+`/guichier/update/`+ops['id'],ops,options);
    }
	
	getReservationsByGuichetier(id:number) {
		console.log(id);
		return this.http.get(this.url+`/reservation/guichier/`+id);
	}
	setUser(user:any) {
		this.user = user;
		localStorage.setItem('User',JSON.stringify(this.user));
	}
	getUser() {
		this.user = JSON.parse(localStorage.getItem('User'));
		return this.user;
	}
	setUserType(t:number) {
		this.user['type'] = t;
		localStorage.setItem('User',JSON.stringify(this.user));
	}
	getUserType() {
		this.user = JSON.parse(localStorage.getItem('User'));
		return this.user['type'];
	}
	redirect(url:string) {
			window.location.href = url;
	}
}