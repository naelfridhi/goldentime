package ensi.pcd.project.utils;

import java.sql.Time;
import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;

import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.service.ETicketService;

class MessagePrinterTask {
	ETicketService eTicketService;

	@Scheduled(fixedDelay = 5000)
	public void executeEveryOneMin() {
		List<ETicket> etickets = eTicketService.getAllETicket();
		for (int i = 0; i < etickets.size(); i++) {
			ETicket ticket = etickets.get(i);
			ticket.setTimeleft(new Time(ticket.getTimeleft().getTime() - 4000));
			eTicketService.updateETicket(ticket);
		}
		System.out.println("Bonjour Mr");
	}

	public ETicketService geteTicketService() {
		return eTicketService;
	}

	public void seteTicketService(ETicketService eTicketService) {
		this.eTicketService = eTicketService;
	}

}