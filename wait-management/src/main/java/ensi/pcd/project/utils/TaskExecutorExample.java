package ensi.pcd.project.utils;

import java.sql.Time;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;

import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.service.ETicketService;

public class TaskExecutorExample implements Runnable {
	@Autowired
	ETicketService eTicketService;

	public ETicketService geteTicketService() {
		return eTicketService;
	}

	public void seteTicketService(ETicketService eTicketService) {
		this.eTicketService = eTicketService;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (eTicketService != null) {
				
			}
		}
	}

	private TaskExecutor taskExecutor;

	public TaskExecutorExample(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}

	public void printMessages() {
		for (int i = 0; i < 25; i++) {
	//		taskExecutor.execute(new MessagePrinterTask("Message" + i));
		}
	}
}
