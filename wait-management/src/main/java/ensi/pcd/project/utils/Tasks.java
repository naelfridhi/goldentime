package ensi.pcd.project.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Notification;
import com.google.android.gcm.server.Sender;

import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.service.ClientService;
import ensi.pcd.project.service.ETicketService;

class Tasks {
	@Autowired
	ETicketService eTicketService;

	@Autowired
	ClientService clientService;

	String serverKey = "AIzaSyAIK4HhDxzifOsn7p9orh5DOFcXcMP6tOo"; //

	@Scheduled(fixedDelay = 2000)
	@Transactional
	public void executeEveryTwoSeconds() {
		eTicketService.downTime(2);
	}

	@Scheduled(fixedDelay = 30000)
	public void executeEveryOneMinForTen() {
		List<ETicket> etickets = eTicketService.getTicketsWithTime("00:10:00", "00:10:59");
		List<String> tokens = new ArrayList<String>();
		for (ETicket e : etickets) {
			if (e.getReservation().getClient().getTokenId() != null)
				tokens.add(e.getReservation().getClient().getTokenId());
		}
		if (!tokens.isEmpty())
			sendNotify(10, tokens);
	}

	@Scheduled(fixedDelay = 30000)
	public void executeEveryOneMinForFive() {
		List<ETicket> etickets = eTicketService.getTicketsWithTime("00:05:00", "00:05:59");
		List<String> tokens = new ArrayList<String>();
		for (ETicket e : etickets) {
			if (e.getReservation().getClient().getTokenId() != null)
				tokens.add(e.getReservation().getClient().getTokenId());
		}
		if (!tokens.isEmpty())
			sendNotify(5, tokens);
	}

	public ETicketService geteTicketService() {
		return eTicketService;
	}

	public void seteTicketService(ETicketService eTicketService) {
		this.eTicketService = eTicketService;
	}

	public void sendNotify(int nbr, List<String> tokens) {
		try {
			Sender sender = new FCMSender(serverKey);
			Notification notify = new Notification.Builder(null).title("Golden Time")
					.body("Il vous reste " + nbr + " minutes pour passer au guichet.").build();
			Message message = new Message.Builder().collapseKey("message").timeToLive(30).delayWhileIdle(true)
					.priority(Message.Priority.HIGH)
					.addData("message", "Il vous reste " + nbr + " minutes pour passer au guichet.")
					.notification(notify).build();
			// Use the same token(or registration id) that was earlier
			// used to send the message to the client directly from
			// Firebase Console's Notification tab.
			// System.out.println("Token_ID = " +
			// e.getReservation().getClient().getTokenId());
			MulticastResult result = sender.send(message, tokens, 1);
			System.out.println("Result: Success = " + result.getSuccess() + "Error = " + result.getFailure() + "Code = "
					+ "String = " + result.toString());
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}
}