package ensi.pcd.project.dao;

import java.util.List;

import ensi.pcd.project.model.Guichier;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.model.Service;

public interface ReservationDao {

	List<Reservation> findAllReservation();

	void save(Reservation reservation);

	void delete(Reservation reservation);

	void update(Reservation reservation);

	Reservation findReservationById(int id);

	Reservation findLastReservationByService(Service service);

	List<Reservation> findReservationsByGuichier(Guichier guichier);
}
