package ensi.pcd.project.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import ensi.pcd.project.dao.GuichierDao;
import ensi.pcd.project.model.Guichier;

public class GuichierDaoImpl implements GuichierDao {

	SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Guichier> findAllGuichier() {
		return getSessionFactory().getCurrentSession().createQuery("from Guichier").list();
	}

	@Override
	public void save(Guichier guichier) {
		getSessionFactory().getCurrentSession().save(guichier);

	}

	@Override
	public void delete(Guichier guichier) {
		getSessionFactory().getCurrentSession().delete(guichier);
	}

	@Override
	public void update(Guichier guichier) {
		getSessionFactory().getCurrentSession().merge(guichier);
	}

	@Override
	public Guichier findGuichierById(int id) {
		return (Guichier) getSessionFactory().getCurrentSession().get(Guichier.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Guichier findGuichierByUsername(String username) {
		List<Guichier> list = getSessionFactory().getCurrentSession()
				.createQuery("from Guichier Where username = :username").setString("username", username).list();
		if (list.isEmpty())
			return null;
		return (Guichier) list.get(0);
	}

}
