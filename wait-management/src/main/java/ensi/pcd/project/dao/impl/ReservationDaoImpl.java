package ensi.pcd.project.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import ensi.pcd.project.dao.ReservationDao;
import ensi.pcd.project.model.Guichier;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.model.Service;

public class ReservationDaoImpl implements ReservationDao {

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Reservation> findAllReservation() {
		return getSessionFactory().getCurrentSession().createQuery("from Reservation").list();
	}

	@SuppressWarnings("unchecked")
	public Reservation findLastReservationByService(Service service) {
		List<Reservation> reservations = getSessionFactory().getCurrentSession()
				.createQuery("from Reservation where service=:service order by dateRetirer desc")
				.setParameter("service", service).list();
		if (reservations.isEmpty())
			return null;
		else
			return reservations.get(0);
	}

	@Override
	public void save(Reservation reservation) {
		getSessionFactory().getCurrentSession().save(reservation);
	}

	@Override
	public void delete(Reservation reservation) {
		getSessionFactory().getCurrentSession().delete(reservation);
	}

	@Override
	public void update(Reservation reservation) {
		getSessionFactory().getCurrentSession().update(reservation);
	}

	@Override
	public Reservation findReservationById(int id) {
		return (Reservation) getSessionFactory().getCurrentSession().get(Reservation.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Reservation> findReservationsByGuichier(Guichier guichier) {
		return getSessionFactory().getCurrentSession()
				.createQuery("from Reservation WHERE guichier = :guichier and eTicket is not null")
				.setParameter("guichier", guichier).list();
	}

}
