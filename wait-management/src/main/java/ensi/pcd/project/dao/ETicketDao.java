package ensi.pcd.project.dao;

import java.util.List;

import ensi.pcd.project.model.ETicket;

public interface ETicketDao {

	List<ETicket> findAllETicket();

	ETicket findETicketById(int id);

	void save(ETicket ETicket);

	void delete(ETicket ETicket);

	void update(ETicket ETicket);

	void updateTime(long i);

	List<ETicket> findTicketsWithTime(String i, String b);
}
