package ensi.pcd.project.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import ensi.pcd.project.dao.ServiceDao;
import ensi.pcd.project.model.Service;

@Repository
public class ServiceDaoImpl implements ServiceDao {

	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Service> findAllServices() {
		return getSessionFactory().getCurrentSession().createQuery("from Service").list();
	}

	public void save(Service service) {
		getSessionFactory().getCurrentSession().save(service);
	}

	public void delete(Service service) {
		getSessionFactory().getCurrentSession().delete(service);
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void update(Service service) {
		getSessionFactory().getCurrentSession().update(service);
	}

	public Service findServiceById(int id) {
		return (Service) getSessionFactory().getCurrentSession().get(Service.class, id);
	}

}
