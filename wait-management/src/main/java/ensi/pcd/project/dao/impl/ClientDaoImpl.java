package ensi.pcd.project.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import ensi.pcd.project.dao.ClientDao;
import ensi.pcd.project.model.Client;

@Repository
public class ClientDaoImpl implements ClientDao {

	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Client> findAllClient() {
		List<Client> list = getSessionFactory().getCurrentSession().createQuery("from Client").list();
		return list;
	}

	public void save(Client client) {
		getSessionFactory().getCurrentSession().save(client);
	}

	public void delete(Client client) {
		getSessionFactory().getCurrentSession().delete(client);
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void update(Client client) {
		getSessionFactory().getCurrentSession().update(client);
	}

	public Client findClientById(int id) {
		return (Client) getSessionFactory().getCurrentSession().get(Client.class, id);
	}

	@SuppressWarnings("unchecked")
	public Client findClientByTokenId(String tokenId) {
		List<Client> clients = getSessionFactory().getCurrentSession().createQuery("from Client where tokenId = :token")
				.setParameter("token", tokenId).list();
		if (clients.isEmpty())
			return null;
		else
			return clients.get(0);
	}

}
