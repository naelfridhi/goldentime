package ensi.pcd.project.dao;

import java.util.List;

import ensi.pcd.project.model.Guichier;

public interface GuichierDao {

	List<Guichier> findAllGuichier();

	void save(Guichier guichier);

	void delete(Guichier guichier);
	
	void update(Guichier guichier);
	
	Guichier findGuichierById(int id);

	Guichier findGuichierByUsername(String username);
}
