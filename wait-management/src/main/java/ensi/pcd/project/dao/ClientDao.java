package ensi.pcd.project.dao;

import java.util.List;

import ensi.pcd.project.model.Client;

public interface ClientDao {

	List<Client> findAllClient();

	void save(Client client);

	void delete(Client client);

	void update(Client client);

	Client findClientById(int id);

	Client findClientByTokenId(String tokenId);


}