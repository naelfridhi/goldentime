package ensi.pcd.project.dao;

import java.util.List;

import ensi.pcd.project.model.Admin;

public interface AdminDao {

	List<Admin> read();
	void create(Admin admin) ;
	void delete(Admin admin) ;
	void update(Admin admin);
	
}
