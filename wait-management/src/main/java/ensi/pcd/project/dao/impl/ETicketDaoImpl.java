package ensi.pcd.project.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import ensi.pcd.project.dao.ETicketDao;
import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.model.Reservation;

public class ETicketDaoImpl implements ETicketDao {
	SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ETicket> findAllETicket() {
		return getSessionFactory().getCurrentSession().createQuery("from ETicket").list();
	}

	@Override
	public void save(ETicket ETicket) {
		getSessionFactory().getCurrentSession().save(ETicket);

	}

	@Override
	@Transactional
	public void delete(ETicket ETicket) {
		Reservation reservation = ETicket.getReservation();
		reservation.seteTicket(null);
		Date date = new Date();
		reservation.setPullDate(date);
		getSessionFactory().getCurrentSession().update(reservation);
		getSessionFactory().getCurrentSession().delete(ETicket);
	}

	@Override
	public void update(ETicket ETicket) {
		getSessionFactory().getCurrentSession().update(ETicket);
	}

	public ETicket findETicketById(int id) {
		return (ETicket) getSessionFactory().getCurrentSession().get(ETicket.class, id);
	}

	public void updateTime(long i) {
		getSessionFactory().getCurrentSession().createSQLQuery(
				"UPDATE eticket SET timeleft = timeleft - interval " + i + " second WHERE TIME(timeleft) > '00:00:00'")
				.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<ETicket> findTicketsWithTime(String i, String j) {
		return getSessionFactory().getCurrentSession()
				.createQuery("from ETicket where time(timeleft) between :time1 and :time2").setString("time1", i)
				.setString("time2", j).list();

	}
}
