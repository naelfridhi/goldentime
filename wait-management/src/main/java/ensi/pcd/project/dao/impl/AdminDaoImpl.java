package ensi.pcd.project.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import ensi.pcd.project.dao.AdminDao;
import ensi.pcd.project.model.Admin;

@Repository
public class AdminDaoImpl implements AdminDao{

	private SessionFactory sessionFactory ;
	
	@SuppressWarnings("unchecked")
	public List<Admin> read() {
		List<Admin> list = getSessionFactory().getCurrentSession().createQuery("from Admin").list();
		return list;
	}

	
	public void create(Admin admin) {
		getSessionFactory().getCurrentSession().save(admin);
	}

	
	public void delete(Admin admin) {
		getSessionFactory().getCurrentSession().delete(admin);
	}

	public void update(Admin admin) {
		getSessionFactory().getCurrentSession().update(admin); 
		
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
