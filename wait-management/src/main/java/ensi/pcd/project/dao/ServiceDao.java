package ensi.pcd.project.dao;

import java.util.List;

import ensi.pcd.project.model.Service;

public interface ServiceDao {
	List<Service> findAllServices();

	void save(Service service);

	void delete(Service service);

	void update(Service service);

	Service findServiceById(int id);
}
