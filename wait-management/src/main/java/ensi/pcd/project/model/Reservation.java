package ensi.pcd.project.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "reservation")
public class Reservation {

	private int id;

	private Date pullDate;

	private Date returnDate;

	private Guichier guichier;

	private ETicket eTicket;

	private Client client;

	private Service service;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "dateRetirer")
	public Date getPullDate() {
		return pullDate;
	}

	public void setPullDate(Date pullDate) {
		this.pullDate = pullDate;
	}

	@Column(name = "dateRetourner")
	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	@ManyToOne(targetEntity = Guichier.class)
	@JoinColumn(name = "guichier_id")
	@JsonIgnoreProperties(value = { "reservations", "service" })
	public Guichier getGuichier() {
		return guichier;
	}

	public void setGuichier(Guichier guichier) {
		this.guichier = guichier;
	}

	@OneToOne(targetEntity = ETicket.class, optional = true)
	@JoinColumn(name = "eticket_id", nullable = true)
	@JsonIgnoreProperties(value = "reservation")
	public ETicket geteTicket() {
		return eTicket;
	}

	public void seteTicket(ETicket eTicket) {
		this.eTicket = eTicket;
	}

	@ManyToOne(targetEntity = Client.class)
	@JoinColumn(name = "client_id")
	@JsonIgnoreProperties(value = "reservations")
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@ManyToOne(targetEntity = Service.class)
	@JoinColumn(name = "service_id")
	@JsonIgnoreProperties(value = "reservations")
	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}
