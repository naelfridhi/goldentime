package ensi.pcd.project.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Guichier extends Utilisateur {

	public String username;

	public String password;

	public Boolean active;

	public Integer counterNumber;

	public Service service;

	private Set<Reservation> reservations;

	@Column(name = "username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password")
	@JsonProperty(access = Access.WRITE_ONLY)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Column(name = "counterNumber")
	public Integer getCounterNumber() {
		return counterNumber;
	}

	public void setCounterNumber(Integer counterNumber) {
		this.counterNumber = counterNumber;
	}

	@ManyToOne
	@JsonIgnoreProperties(value = "guichiers")
	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@OneToMany(targetEntity = Reservation.class, mappedBy = "guichier", cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JsonIgnoreProperties(value = "guichier")
	public Set<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}
}
