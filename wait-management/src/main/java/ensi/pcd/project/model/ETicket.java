package ensi.pcd.project.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "eticket")
public class ETicket {
	private int id;

	private int rank;

	private Time timeleft;

	private Reservation reservation;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "rank")
	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	@Column(name = "timeleft")
	public Time getTimeleft() {
		return timeleft;
	}

	public void setTimeleft(Time timeleft) {
		this.timeleft = timeleft;
	}

	@OneToOne(targetEntity = Reservation.class, mappedBy = "eTicket", fetch = FetchType.EAGER)
	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

}
