package ensi.pcd.project.model;

import java.sql.Time;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "service")
public class Service {

	private int id;

	private String name;

	private String description;

	private Time serviceTime;

	private Set<Reservation> reservations;

	private Set<Guichier> Guichiers;

	@OneToMany(targetEntity = Guichier.class, mappedBy = "service", fetch = FetchType.EAGER)
	@JsonIgnoreProperties(value = "service")
	public Set<Guichier> getGuichiers() {
		return Guichiers;
	}

	public void setGuichiers(Set<Guichier> guichiers) {
		Guichiers = guichiers;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(targetEntity = Reservation.class, mappedBy = "service", cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	public Set<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Column(name = "service_time")
	public Time getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(Time serviceTime) {
		this.serviceTime = serviceTime;
	}

}
