package ensi.pcd.project.service.impl;

import java.util.List;

import ensi.pcd.project.dao.AdminDao;
import ensi.pcd.project.model.Admin;
import ensi.pcd.project.service.AdminService;

public class AdminServiceImpl implements AdminService{

	AdminDao adminDao ;
	
	public List<Admin> getAllAdmin() {
		return adminDao.read();
	}

	public void addAdmin(Admin admin) {
		adminDao.create(admin);
	}

	
	public void deleteAdmin(Admin admin) {
		adminDao.delete(admin);
	}

	public AdminDao getAdminDao() {
		return adminDao;
	}

	public void setAdminDao(AdminDao adminDao) {
		this.adminDao = adminDao;
	}

}
