package ensi.pcd.project.service;

import java.util.List;

import ensi.pcd.project.model.Client;

public interface ClientService {

	List<Client> getAllClient();

	void addClient(Client client);

	void deleteClient(Client client);

	Client getClientById(int id);

	Client getClientByTokenId(String tokenId);

}
