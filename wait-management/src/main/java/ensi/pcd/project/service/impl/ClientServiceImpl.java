package ensi.pcd.project.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import ensi.pcd.project.dao.ClientDao;
import ensi.pcd.project.model.Client;
import ensi.pcd.project.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	ClientDao clientDao;

	public List<Client> getAllClient() {
		return clientDao.findAllClient();
	}

	public Client getClientById(int id) {
		return clientDao.findClientById(id);
	}

	public Client getClientByTokenId(String tokenId) {
		return clientDao.findClientByTokenId(tokenId);
	}

	public void addClient(Client client) {
		clientDao.save(client);
	}

	public void deleteClient(Client client) {
		clientDao.delete(client);
	}

	public ClientDao getClientDao() {
		return clientDao;
	}

	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

}
