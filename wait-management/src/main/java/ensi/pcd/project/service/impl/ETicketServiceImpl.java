package ensi.pcd.project.service.impl;

import java.util.List;

import ensi.pcd.project.dao.ETicketDao;
import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.service.ETicketService;

public class ETicketServiceImpl implements ETicketService {

	private ETicketDao eTicketDao;

	@Override
	public List<ETicket> getAllETicket() {
		return eTicketDao.findAllETicket();
	}

	@Override
	public ETicket getETicketById(int id) {
		return eTicketDao.findETicketById(id);
	}

	@Override
	public void addETicket(ETicket eTicket) {
		eTicketDao.save(eTicket);
	}

	@Override
	public void updateETicket(ETicket eTicket) {
		eTicketDao.update(eTicket);
	}

	@Override
	public void deleteETicket(ETicket eTicket) {
		eTicketDao.delete(eTicket);
	}

	public ETicketDao geteTicketDao() {
		return eTicketDao;
	}

	public void seteTicketDao(ETicketDao eTicketDao) {
		this.eTicketDao = eTicketDao;
	}

	@Override
	public void downTime(long i) {
		eTicketDao.updateTime(i);
	}

	@Override
	public List<ETicket> getTicketsWithTime(String i, String b) {
		return eTicketDao.findTicketsWithTime(i, b);
	}

}
