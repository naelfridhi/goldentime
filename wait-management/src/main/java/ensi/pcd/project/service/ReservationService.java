package ensi.pcd.project.service;

import java.util.List;

import ensi.pcd.project.model.Guichier;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.model.Service;

public interface ReservationService {
	List<Reservation> getAllReservation();

	void addReservation(Reservation reservation);

	void updateReservation(Reservation reservation);

	void deleteReservation(Reservation reservation);

	Reservation getReservationById(int id);

	Reservation getLastReservationByService(Service service);

	List<Reservation> getReservationsByGuichier(Guichier guichier);
}
