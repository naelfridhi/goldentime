package ensi.pcd.project.service;

import java.util.List;

import ensi.pcd.project.model.Admin;

public interface AdminService {

	List<Admin> getAllAdmin();

	void addAdmin(Admin admin);

	void deleteAdmin(Admin admin);
}
