package ensi.pcd.project.service;

import java.util.List;

import ensi.pcd.project.model.Guichier;

public interface GuichierService {
	List<Guichier> getAllGuichier();

	void addGuichier(Guichier guichier);

	void updateGuichier(Guichier guichier);

	void deleteGuichier(Guichier guichier);

	Guichier getGuichierById(int id);

	Guichier getGuichierByUsername(String username);

	Guichier getGuichierByUsername(String username, String password);

}
