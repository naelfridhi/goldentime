package ensi.pcd.project.service;

import java.util.List;

import ensi.pcd.project.model.Service;

public interface ServiceService {
	List<Service> getAllServices();

	void addService(Service service);

	void deleteService(Service service);

	void updateService(Service service);

	Service getServiceById(int id);
}
