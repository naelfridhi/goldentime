package ensi.pcd.project.service.impl;

import java.util.List;

import ensi.pcd.project.dao.ReservationDao;
import ensi.pcd.project.model.Guichier;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.model.Service;
import ensi.pcd.project.service.ReservationService;

public class ReservationServiceImpl implements ReservationService {

	ReservationDao reservationDao;

	@Override
	public List<Reservation> getAllReservation() {
		return reservationDao.findAllReservation();
	}

	@Override
	public void addReservation(Reservation reservation) {
		reservationDao.save(reservation);
	}

	@Override
	public void updateReservation(Reservation reservation) {
		reservationDao.update(reservation);
	}

	@Override
	public void deleteReservation(Reservation reservation) {
		reservationDao.delete(reservation);
	}

	public ReservationDao getReservationDao() {
		return reservationDao;
	}

	public void setReservationDao(ReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}

	public Reservation getReservationById(int id) {
		return reservationDao.findReservationById(id);
	}

	public Reservation getLastReservationByService(Service service) {
		return reservationDao.findLastReservationByService(service);
	}

	public List<Reservation> getReservationsByGuichier(Guichier guichier) {
		return reservationDao.findReservationsByGuichier(guichier);
	}
}
