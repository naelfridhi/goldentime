package ensi.pcd.project.service.impl;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import ensi.pcd.project.dao.GuichierDao;
import ensi.pcd.project.model.Guichier;
import ensi.pcd.project.service.GuichierService;

public class GuichierServiceImpl implements GuichierService {

	private GuichierDao guichierDao;

	public List<Guichier> getAllGuichier() {
		return guichierDao.findAllGuichier();
	}

	@Override
	public void addGuichier(Guichier guichier) {
		guichierDao.save(guichier);
	}

	@Override
	public void updateGuichier(Guichier guichier) {
		Guichier g = guichierDao.findGuichierById(guichier.getId());
		guichier.setPassword(g.getPassword());
		guichier.setService(g.getService());
		guichierDao.update(guichier);
	}

	@Override
	public void deleteGuichier(Guichier guichier) {
		guichierDao.delete(guichier);
	}

	public GuichierDao getGuichierDao() {
		return guichierDao;
	}

	public void setGuichierDao(GuichierDao guichierDao) {
		this.guichierDao = guichierDao;
	}

	public Guichier getGuichierById(int id) {
		return guichierDao.findGuichierById(id);
	}

	@Override
	public Guichier getGuichierByUsername(String username) {
		return guichierDao.findGuichierByUsername(username);
	}

	@Override
	public Guichier getGuichierByUsername(String username, String password) {
		Guichier g = guichierDao.findGuichierByUsername(username);
		if (g == null)
			return null;
		String hash = DigestUtils.md5Hex(password);
		if (g.getPassword().equals(hash)) {
			return g;
		}
		return null;
	}

}
