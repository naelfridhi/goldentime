package ensi.pcd.project.service;

import java.util.List;

import ensi.pcd.project.model.ETicket;

public interface ETicketService {
	List<ETicket> getAllETicket();

	ETicket getETicketById(int id);

	void addETicket(ETicket eTicket);

	void updateETicket(ETicket eTicket);

	void deleteETicket(ETicket eTicket);

	void downTime(long i);

	List<ETicket> getTicketsWithTime(String i, String b);
}
