package ensi.pcd.project.service.impl;

import java.util.List;

import ensi.pcd.project.dao.ServiceDao;
import ensi.pcd.project.model.Service;
import ensi.pcd.project.service.ServiceService;

public class ServiceServiceImpl implements ServiceService {
	private ServiceDao serviceDao;

	public List<Service> getAllServices() {
		return serviceDao.findAllServices();
	}

	public void addService(Service service) {
		serviceDao.save(service);
	}

	public void deleteService(Service service) {
		serviceDao.delete(service);
	}

	public ServiceDao getServiceDao() {
		return serviceDao;
	}

	public void setServiceDao(ServiceDao serviceDao) {
		this.serviceDao = serviceDao;
	}

	public void updateService(Service service) {
		serviceDao.update(service);
	}

	public Service getServiceById(int id) {
		return serviceDao.findServiceById(id);
	}

}
