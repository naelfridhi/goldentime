package ensi.pcd.project.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.service.ETicketService;
import ensi.pcd.project.service.ReservationService;

@Controller
public class ETicketController {

	@Autowired
	private ETicketService eTicketService;

	@Autowired
	private ReservationService reservationService;

	@RequestMapping(value = "/eticket", method = RequestMethod.GET)
	@ResponseBody
	public List<ETicket> allETickets(ModelMap pMap) {
		return eTicketService.getAllETicket();
	}

	@RequestMapping(value = "/eticket/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ETicket eTicket(@PathVariable(value = "id") int id) {
		return eTicketService.getETicketById(id);
	}

	@RequestMapping(value = "/eticket/save", method = RequestMethod.POST)
	@ResponseBody
	public ETicket saveETicket(@Valid ETicket eTicket) {
		eTicketService.addETicket(eTicket);
		return eTicket;
	}

	@RequestMapping(value = "/eticket/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public ETicket updateETicket(@Valid ETicket eTicket) {
		eTicketService.updateETicket(eTicket);
		return eTicket;
	}

	@RequestMapping(value = "/eticket/delete/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ETicket deleteETicket(@PathVariable(value = "id") int id) {
		ETicket eticket = eTicketService.getETicketById(id);
		Reservation reservation = eticket.getReservation();
		reservation.setReturnDate(new Date());
		eTicketService.deleteETicket(eticket);
		return null;
	}
}
