package ensi.pcd.project.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ensi.pcd.project.model.Service;
import ensi.pcd.project.service.ServiceService;

@Controller
public class ServiceController {

	@Autowired
	private ServiceService serviceService;

	@RequestMapping(value = "/service", method = RequestMethod.GET)
	@ResponseBody
	public List<Service> allServices() {
		return serviceService.getAllServices();
	}

	@RequestMapping(value = "/service/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Service service(@PathVariable(value = "id") int id) {
		return serviceService.getServiceById(id);
	}

	@RequestMapping(value = "/service/save", method = RequestMethod.POST)
	@ResponseBody
	public Service saveservice(@Valid Service service) {
		serviceService.addService(service);
		return service;
	}

	@RequestMapping(value = "/service/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public Service updateservice(@Valid Service service) {
		serviceService.updateService(service);
		return service;
	}

	@RequestMapping(value = "/service/delete/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Service deleteservice(@PathVariable(value = "id") int id) {
		serviceService.deleteService(serviceService.getServiceById(id));
		return null;
	}
}
