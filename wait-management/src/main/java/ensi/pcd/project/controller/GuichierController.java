package ensi.pcd.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ensi.pcd.project.model.Guichier;
import ensi.pcd.project.service.GuichierService;

@Controller
@RequestMapping(value = "/guichier")
public class GuichierController {

	@Autowired
	private GuichierService guichierService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Guichier> allGuichier() {
		return guichierService.getAllGuichier();
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ResponseBody
	public Guichier Login(@Valid @RequestBody Guichier guichier, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Guichier g = guichierService.getGuichierByUsername(guichier.getUsername(), guichier.getPassword());
		if (g == null) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().print("{}");
		}
		return g;

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Guichier guichier(@PathVariable(value = "id") int id) {
		return guichierService.getGuichierById(id);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public Guichier saveguichier(@Valid Guichier guichier) {
		guichierService.addGuichier(guichier);
		return guichier;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public Guichier updateguichier(@Valid @RequestBody Guichier guichier) {
		guichierService.updateGuichier(guichier);
		return guichier;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Guichier deleteguichier(@PathVariable(value = "id") int id) {
		guichierService.deleteGuichier(guichierService.getGuichierById(id));
		return null;
	}
}
