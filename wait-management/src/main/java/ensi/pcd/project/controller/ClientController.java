package ensi.pcd.project.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ensi.pcd.project.model.Client;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.service.ClientService;

@RestController
@RequestMapping(value = "/client")
public class ClientController {

	@Autowired
	ClientService clientService;

	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	@ResponseBody
	public List<Client> getClient() {
		List<Client> clients = clientService.getAllClient();
		return clients;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Client getClientFromId(@PathVariable(value = "id") int id, HttpServletResponse response) throws IOException {
		Client client = clientService.getClientById(id);
		if (client == null) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().print("{}");
		}
		return client;
	}

	@RequestMapping(value = "/token/{tokenId}", method = RequestMethod.GET)
	@ResponseBody
	public Client getClientFromTokenId(@PathVariable(value = "tokenId") String tokenId, HttpServletResponse response)
			throws IOException {
		Client client = clientService.getClientByTokenId(tokenId);
		if (client == null) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().print("{}");
		}
		return client;
	}

	@RequestMapping(value = "/{id}/reservations", method = RequestMethod.GET)
	@ResponseBody
	public Set<Reservation> getReservationByClient(@PathVariable(value = "id") int id, HttpServletResponse response)
			throws IOException {
		@SuppressWarnings("unchecked")
		Set<Reservation> reservations = clientService.getClientById(id).getReservations();
		if (reservations == null) {
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().print("{}");
		}
		return reservations;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Client addClientAction(@Valid @RequestBody Client client) {
		clientService.addClient(client);
		return client;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String deleteClient(HttpServletRequest request) {
		return "client";
	}
}
