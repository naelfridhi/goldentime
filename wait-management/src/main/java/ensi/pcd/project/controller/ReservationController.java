package ensi.pcd.project.controller;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ensi.pcd.project.model.Client;
import ensi.pcd.project.model.ETicket;
import ensi.pcd.project.model.Reservation;
import ensi.pcd.project.model.Service;
import ensi.pcd.project.service.ClientService;
import ensi.pcd.project.service.ETicketService;
import ensi.pcd.project.service.GuichierService;
import ensi.pcd.project.service.ReservationService;
import ensi.pcd.project.service.ServiceService;

@Controller
public class ReservationController {

	@Autowired
	private ReservationService reservationService;

	@Autowired
	private ClientService clientService;

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private ETicketService eTicketService;

	@Autowired
	private GuichierService guichierService;

	@RequestMapping(value = "/reservation", method = RequestMethod.GET)
	@ResponseBody
	public List<Reservation> allReservations(ModelMap pMap) {
		return reservationService.getAllReservation();
	}

	@RequestMapping(value = "/reservation/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Reservation reservation(@PathVariable(value = "id") int id) {
		return reservationService.getReservationById(id);
	}

	@RequestMapping(value = "/reservation/save/{clientId}/{serviceId}", method = RequestMethod.POST)
	@ResponseBody
	public Reservation saveReservation(@Valid Reservation reservation, @PathVariable(value = "clientId") int clientId,
			@PathVariable(value = "serviceId") int serviceId) {
		Client client = clientService.getClientById(clientId);
		Service service = serviceService.getServiceById(serviceId);
		Reservation lastReservation = reservationService.getLastReservationByService(service);
		ETicket eTicket = new ETicket();
		if (lastReservation == null || lastReservation.geteTicket() == null) {
			eTicket.setRank(1);
			eTicket.setTimeleft(service.getServiceTime());
		} else {
			eTicket.setRank(lastReservation.geteTicket().getRank() + 1);
			@SuppressWarnings("deprecation")
			Time time = new Time(
					lastReservation.geteTicket().getTimeleft().getHours() + service.getServiceTime().getHours(),
					lastReservation.geteTicket().getTimeleft().getMinutes() + service.getServiceTime().getMinutes(),
					lastReservation.geteTicket().getTimeleft().getSeconds() + service.getServiceTime().getSeconds());
			eTicket.setTimeleft(time);
		}
		reservation.setClient(client);
		reservation.setService(service);
		reservation.seteTicket(eTicket);
		eTicketService.addETicket(eTicket);
		Date date = new Date();
		reservation.setPullDate(date);
		reservationService.addReservation(reservation);
		return reservation;
	}

	@RequestMapping(value = "/reservation/guichier/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Reservation> reservationByGuichier(@PathVariable(value = "id") int id) {
		return reservationService.getReservationsByGuichier(guichierService.getGuichierById(id));
	}

	@RequestMapping(value = "/reservation/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public Reservation updatereservation(@Valid Reservation reservation) {
		reservationService.updateReservation(reservation);
		return reservation;
	}

	@RequestMapping(value = "/reservation/delete/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Reservation deleteReservation(@PathVariable(value = "id") int id) {
		reservationService.deleteReservation(reservationService.getReservationById(id));
		return null;
	}
}
